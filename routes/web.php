<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\PagesController;
use \App\Http\Controllers\TeamController;
use \App\Http\Controllers\PriorityController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function() {
//    Route::resource('teams', TeamController::class);

    Route::get('/teams', [TeamController::class, 'index']); //Not in use.
    Route::post('/teams/{team}', [TeamController::class, 'store'])->name("teams.store"); //Ryan
    Route::get('/teams/create', [TeamController::class, 'create'])->name("teams.create"); //Ryan

    Route::get('/home', [\App\Http\Controllers\HomeController::class, 'index'])->name('home'); //Ryan

    Route::get('/teams/{team}', [TeamController::class, 'show'])->name("teams.show"); //Working on.
    Route::PUT('/teams/{team}', [TeamController::class, 'update'])->name("teams.update");; //Working on.
    Route::delete('/teams/{team}', [TeamController::class, 'destroy'])->name("teams.destroy"); //To do.
    Route::get('/teams/{team}/edit', [TeamController::class, 'edit']); //Not in use.

    Route::get('/', [PagesController::class, 'home']);
    Route::get('board', [PagesController::class, 'board']);

});

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('logout', [PagesController::class, 'logout'])->name('logout');

//Route::resource('priorities',[PriorityController::class]);

Route::get('priority-show',[PriorityController::class, 'show']);//done
Route::get('priorities',[PriorityController::class,'index'])->name('priorities.index');//done
Route::get('priorities/create',[PriorityController::class,'create'])->name('priorities.create');//done
Route::post('priorities',[PriorityController::class,'store'])->name('priorities.store');//done
Route::get('priorities/{priority}',[PriorityController::class,'show'])->name('priorities.show');//done
Route::get('priorities/{priority}/edit',[PriorityController::class,'edit'])->name('priorities.edit');//done
Route::patch('priorities/{priority}',[PriorityController::class,'update'])->name('priorities.update');//done
Route::delete('priorities/{priority}',[PriorityController::class,'destroy'])->name('priorities.delete');//done

Route::get('/retrospective/create', [App\Http\Controllers\RetrospectiveController::class, 'create'])->name('retrospective.create');
Route::get('/retrospective/{retrospective}', [App\Http\Controllers\RetrospectiveController::class, 'index']);
Route::post('/retrospective', [App\Http\Controllers\RetrospectiveController::class, 'store']);

Route::get('tasks/create/{retrospective}/{column}', [App\Http\Controllers\TaskController::class, 'create']);
Route::post('/tasks', [App\Http\Controllers\TaskController::class, 'store']);
Route::get('tasks/{task}/edit', [App\Http\Controllers\TaskController::class, 'edit']);
Route::patch('tasks/{task}', [App\Http\Controllers\TaskController::class, 'update']);
Route::delete('tasks/{task}', [App\Http\Controllers\TaskController::class, 'destroy']);
