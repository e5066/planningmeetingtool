<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    use HasFactory;

    protected $fillable = [
      'name'
    ];

    public function retrospectives()
    {
        return $this->hasMany(Retrospective::class);
    }

    public function templateColumns()
    {
        return $this->hasMany(TemplateColumn::class);
    }
}
