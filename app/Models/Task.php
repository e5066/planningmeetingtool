<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
      'title','description'
    ];

    public function priority()
    {
        return $this->belongsTo(Priority::class);
    }

    public function retrospective()
    {
        return $this->belongsTo(Retrospective::class);
    }

    public static function showTask(){
        return DB::table('tasks')
            ->select('tasks.id')
            ->addSelect('tasks.title')
            ->where('tasks.user_id', Auth::id())
            ->get();
    }

    public function users(){
        return $this->belongsTo(User::class);
    }

    public function template_columns(){
        return $this->belongsTo(TemplateColumn::class);
    }
}
