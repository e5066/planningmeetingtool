<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TemplateColumn extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'order'
    ];

    public function template()
    {
        return $this->belongsTo(Template::class);
    }

    public function tasks(){
        return $this->hasMany(Task::class);
    }

}
