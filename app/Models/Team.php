<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Team extends Model
{
        use HasFactory;

    protected $fillable = [
        'name'
    ];

    public function users()
    {
        return $this->belongsToMany(
            User::class,
            'teams_users',
            'user_id',
            'team_id');
    }

    public function retrospectives()
    {
        return $this->hasMany(Retrospective::class);
    }
    public static function showJoinedTeam(){
        return DB::table('teams')
            ->select('teams.name')
            ->addSelect('teams.id')
            ->where('teams_users.user_id', Auth::id())
            ->join('teams_users', 'teams.id', '=', 'teams_users.team_id')
            ->get();
    }

    public static function getTeam($team){

        $currentTeam = DB::table('teams')
            ->select('teams.name as teamName')
            ->addSelect('teams.id')
            ->where('teams_users.user_id', Auth::id())
            ->where('teams.name', $team)
            ->join('teams_users', 'teams.id', '=', 'teams_users.team_id')
            ->get();

        if ($currentTeam->count() >= 1){

            $teamMembers = DB::table('teams_users')
                ->select('teams_users.user_id')
                ->addSelect('users.name as name')
                ->addSelect('isAdmin')
                ->where('teams_users.team_id', $currentTeam[0]->id)
                ->join('users', 'teams_users.user_id', '=', 'users.id')
                ->get();

            $icon = substr($currentTeam[0]->teamName, 0);

            return [
                'id' => $currentTeam[0]->id,
                'name' => $currentTeam[0]->teamName,
                'letterIcon' => $icon[0],
                'members' => $teamMembers
            ];
        }
        else{
            return abort(401);
        }
        //return view('teams.teams', ['data' => $data]);
    }

    public static function subOnTeams(){
        return Team::select('teams.id')
            ->addSelect('teams.name')
            ->where('teams_users.user_id', Auth::id())
            ->join('teams_users', 'teams.id' , '=', 'teams_users.team_id')
            ->get();
    }

    public static function retrosOnTeam($teamId){
        return Retrospective::select('retrospectives.name as retroName')
            ->addSelect('retrospectives.id')
            ->addSelect('templates.name as templateName')
            ->where('retrospectives.team_id', $teamId)
            ->join('templates', 'retrospectives.template_id', '=', 'templates.id')
            ->get();

    }
}
