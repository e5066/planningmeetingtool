<?php

namespace App\Http\Controllers;

use App\Models\Retrospective;
use App\Models\Task;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Template;
use http\Env\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($retrospective,$column)
    {
        $user = Auth::id();

        $retname = $this->getInfoOfRetro($retrospective);
        $priorities = $this->getPrioritiesOfRetro($retrospective);
        $columns = $this->getColumnsOfTemplate($retname[0]->template_id);

        return view('tasks.create_tasks', ['retrospective' =>  $retrospective, 'priorities' => $priorities , 'columns' => $columns, 'retname' => $retname]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTaskRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTaskRequest $request)
    {
        $task = new Task();

        $task->title = $request->title;
        $task->description = $request->desc;
        $task->template_column = $request->kolom;
        $task->retrospective_id = $request->retid;
        $task->user_id = Auth::id();
        //Defaulting priorityid & userid when empty to 1 for now to prevent cannot be null
        if (isset($request->priority_id)){
            $task->priority_id = $request->priority_id;
        }
        else{
            $task->priority_id = 1;
        }

        $task->save();

        $returnRetro = $this->getInfoOfRetro($request->retid);

        //die(var_dump($returnRetro));

        return redirect('retrospective/'.$returnRetro[0]->name);
    }

    public function getPrioritiesOfRetro($id){
        return DB::table('priorities')
            ->where('team_id', '=', $id)
            ->get();
    }

    public function getInfoOfRetro($id){
        return DB::table('retrospectives')
            ->where('id', '=', $id)
            ->get();
    }

    public function getColumnsOfTemplate($id){
        return DB::table('template_columns')
            ->where('template_id', '=', $id)
            ->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit($task)
    {
        $user = Auth::id();
        $taskinfo = $this->getRetroOfTask($task);
        $priorities = $this->getPrioritiesOfRetro($taskinfo[0]->retrospective_id);
        $retname = $this->getInfoOfRetro($taskinfo[0]->retrospective_id);
        $columns = $this->getColumnsOfTemplate($retname[0]->template_id);


        //die(var_dump($retname));

        return view('tasks.edit_tasks', ['priorities' => $priorities, 'columns' => $columns, 'retname' => $retname, 'taskinfo' => $taskinfo]);
        //
    }

    public function getRetroOfTask($id){
        return DB::table('tasks')
            ->where('retrospective_id', '=', $id)
            ->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTaskRequest  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTaskRequest $request, Task $task)
    {
        DB::table('tasks')
            ->where('id', $request->updid)
            ->update(['title' => $request->title, 'description' => $request->desc, 'template_column' => $request->kolom]);

        $returnRetro = $this->getInfoOfRetro($request->retid);

        return redirect('retrospective/'.$returnRetro[0]->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $retro = $this->getInfoOfRetro($task->retrospective_id);

        DB::table('tasks')
            ->where('id', $task->id)
            ->delete();

        return redirect('retrospective/'.$retro[0]->name);
    }
}
