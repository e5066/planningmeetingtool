<?php

namespace App\Http\Controllers;

use App\Models\Priority;
use App\Http\Requests\StorePriorityRequest;
use App\Http\Requests\UpdatePriorityRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use MongoDB\Driver\Session;

class PriorityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public static $currentTeamName;

    public function index()
    {
        /*$priorities=Priority::orderBy('updated_at','desc')->take(5)->get();
        return view('priorities.priority-show',compact('priorities'));*/
        return view('priorities.priority-show',
            ['priorities'=>Priority::orderBy('id','Asc')->get()]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('priorities.priorities');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePriorityRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePriorityRequest $request)
    {
        $priority= new Priority();
        $priority->name=request('name');
        $priority->color=request('color');
        $priority->team_id=request('teamId');
        $priority->save();
        return redirect('/teams/' . $request->teamName);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Priority  $priority
     * @return \Illuminate\Http\Response
     */
    public function show(Priority $priority)
    {
        $priorities=Priority::find($priority);
        return view('priorities.priority-show',compact('priorities'));
        //return view('priorities.priority-show', ['data' => $data['priorities']]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Priority  $priority
     * @return \Illuminate\Http\Response
     */
    public function edit(Priority $priority)
    {
        $priority=Priority::find($priority);
        return view('priorities/priority-edit',compact('priority'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePriorityRequest  $request
     * @param  \App\Models\Priority  $priority
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePriorityRequest $request, Priority $priority)
    {
            $priority=Priority::find($priority->id);
            $priority->name=$request->name;
            $priority->color=$request->color;
            $priority->save();
            return redirect('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Priority  $priority
     * @return \Illuminate\Http\Response
     */
    public function destroy(Priority $priority)
    {
        $priority=Priority::find($priority->id)->delete();
        return redirect('home');
    }

    public static function getTempPriorities(){
        return DB::table('priorities')
            ->select('name','id')
            ->addSelect('color')
            ->get();
    }
}
