<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Http\Requests\StoreTeamRequest;
use App\Http\Requests\UpdateTeamRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\PriorityController;
use MongoDB\Driver\Session;


class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->validate([
            'teamName' => 'required|max:50'

        ]);

        $team = new Team();
        $team->name = $request->teamName;
        $team->save();
        $team->refresh();
        DB::table('teams_users')
            ->insert(['user_id' => Auth::id(), 'team_id' => $team->id, 'isAdmin' => 1]);
        return redirect('/teams/' . $team->name);
    }

/*    public function showTeam($team)
    {
        $data = ['teamInfo' => Team::getTeam($team),'teamJoined' => Team::showJoinedTeam($team), 'allUsers' => User::all()];
        return view('teams.teams_overview', ['teams' => $data]);
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTeamRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTeamRequest $request)
    {
        $user = DB::table('teams_users')
            ->where([['user_id', '=', Auth::id()], ['team_id', '=', $request->teamId]])
            ->first();

        if ($user->isAdmin == 1){
            if ($request->addMember == "Add to Team"){
                DB::table('teams_users')->insert([
                    'team_id' => $request->teamId,
                    'user_id' => $request->selectedUser,
                    'isAdmin' => 0,
                    'created_at' => new \DateTime('now'),
                    'updated_at' => new \DateTime('now')
                ]);
                return redirect()->route('teams.show', $request->teamName);
            }
            else{
                $team = new Team();
                $team->name = $request->input('teamName');
                $team->save();
                return redirect('/');
            }
        }
        else{
            return redirect()->route('teams.show', $request->teamName)->with('failed', "You're not permitted to use this function.");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show($team)
    {
        $data = [
            'teamInfo' => Team::getTeam($team),
            'allUsers' => User::all(),
            'priorities' => PriorityController::getTempPriorities(),
        ];
        return view('teams.teams', [
            'teams' => $data,
            'userInArr' => array_search(Auth::id(), array_column(array($data['teamInfo']['members'][0]), 'user_id')),
            'subOnTeams' => Team::subOnTeams(),
            'subsOnRetro' => Team::retrosOnTeam($data['teamInfo']['id'])
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTeamRequest  $request
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTeamRequest $request, Team $team)
    {
        $oldTeamName = $team->name;
        if ($request->isAdmin == 1) {
            if (!empty($request) && $request->teamName != null) {
                if ($request->teamName != $oldTeamName) {
                    $team->name = $request->teamName;
                    $team->save();
                    $team->refresh();
                    return redirect('/teams/' . $team->name)->with('success', 'Team - ' . $oldTeamName . ' has been updated with a new name called: ' . $team->name);
                } else {
                    return redirect('/teams/' . $oldTeamName)->with('failed', 'Trying to change the team name with the same name? We cannot accept that :)');
                }
            } elseif (!empty($request) && $request->makeAdmin == "Make admin") {
                DB::table('teams_users')
                    ->where([
                        ['user_id', $request->userId],
                        ['team_id', $team->id],
                    ])
                    ->update(['isAdmin' => 1]);
                return redirect('/teams/' . $team->name)->with('success', $request->userName . ' is now a admin, edit enabled.');
            } else {
                return redirect('/teams/' . $oldTeamName)->with('failed', 'Saving is not possible, when there are no changes.');
            }
        }
        else{
            return redirect()->route('teams.show', $team->name)->with('failed', "Your not permitted to use this function.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Team $team)
    {
        if ($request->isAdmin == 1) {
            if (!empty($request) && $request->removeUser == "Remove") {
                DB::table('teams_users')
                    ->where([
                        ['user_id', $request->userId],
                        ['team_id', $team->id],
                    ])->delete();
                return redirect('/teams/' . $team->name)->with('success', $request->userName . ' has been removed from team: ' . $team->name);
            } else {
                $oldName = $team->name;
                $team->destroy($team->id);
                return redirect('/')->with('success', 'Deletion of team - ' . $oldName . ' was successful.');
            }
        }
        else{
            return redirect()->route('teams.show', $team->name)->with('failed', "Your not permitted to use this function.");
        }
    }
}
