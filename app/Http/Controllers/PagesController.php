<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class PagesController extends Controller
{
    public function home(){
        return redirect(route('home'));
    }

    public function logout() {
        Auth::logout();

        return redirect('/');
    }

    public function addTeam(){
        return view('admin.teams.add_team');

    }

    public function addUser(){
        return view('admin.teams.add_user');
    }

}
