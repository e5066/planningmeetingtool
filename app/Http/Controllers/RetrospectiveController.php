<?php

namespace App\Http\Controllers;

use App\Models\Retrospective;
use App\Http\Requests\StoreRetrospectiveRequest;
use App\Http\Requests\UpdateRetrospectiveRequest;
use App\Models\Team;
use App\Models\Template;
use App\Models\TemplateColumn;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RetrospectiveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index($retrospective)
    {
        $retroData = Retrospective::where('name', '=', $retrospective)->firstOrFail();
        $columns = $this->getColumnsPerTemplate($retroData->template_id);
        $tasks = $this->getTasksOfRetro($retroData->id);

        return view('retrospective.retrospective', ['retroData' =>  $retroData, 'columns' => $columns, 'tasks' => $tasks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $templates = Template::all();
        $user = Auth::id();

        $teams = $this->getTeamsPerUser($user);

        return view('retrospective.create_retrospective', ['templates' => $templates, 'teams' => $teams]);
    }

    public function getTeamsPerUser($user) {
        $teamsUsers = DB::table('teams')
            ->where('user_id', '=', $user)
            ->leftJoin('teams_users', 'team_id', '=', 'teams.id')
            ->get();

        return $teamsUsers;
    }

    public function getTasksOfRetro($retrospective){
        return DB::table('tasks')
            ->where('retrospective_id', '=', $retrospective)
            ->join('priorities', 'priority_id', '=', 'priorities.id')
            ->get();
    }

    public function getColumnsPerTemplate($template) {
        return DB::table('template_columns')
            ->where('template_id', '=', $template)
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreRetrospectiveRequest  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(StoreRetrospectiveRequest $request)
    {
        $retrospective = new Retrospective();

        $retrospective->name = $request->name;
        $retrospective->template_id = $request->template;
        $retrospective->team_id = $request->team;

        $retrospective->save();

        return redirect('retrospective');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Retrospective  $retrospective
     * @return \Illuminate\Http\Response
     */
    public function show(Retrospective $retrospective)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Retrospective  $retrospective
     * @return \Illuminate\Http\Response
     */
    public function edit(Retrospective $retrospective)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRetrospectiveRequest  $request
     * @param  \App\Models\Retrospective  $retrospective
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRetrospectiveRequest $request, Retrospective $retrospective)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Retrospective  $retrospective
     * @return \Illuminate\Http\Response
     */
    public function destroy(Retrospective $retrospective)
    {
        //
    }

    public static function firstPossibleRetro(){
        return Retrospective::select('name')
            ->where('tasks.user_id', Auth::id())
            ->join('tasks', 'retrospectives.id', '=', 'tasks.retrospective_id')
            ->first();
    }

    public static function firstPossibleTeam(){

        return Team::select('name')
            ->where('teams_users.user_id', '=', Auth::id())
            ->join('teams_users', 'teams.id', '=', 'teams_users.team_id')
            ->orderBy('teams_users.created_at', 'DESC')
            ->first();
    }
}
