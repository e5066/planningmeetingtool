<?php

namespace App\Http\Controllers;

use App\Models\TemplateColumn;
use App\Http\Requests\StoreTemplateColumnRequest;
use App\Http\Requests\UpdateTemplateColumnRequest;

class TemplateColumnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTemplateColumnRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTemplateColumnRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TemplateColumn  $templateColumn
     * @return \Illuminate\Http\Response
     */
    public function show(TemplateColumn $templateColumn)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TemplateColumn  $templateColumn
     * @return \Illuminate\Http\Response
     */
    public function edit(TemplateColumn $templateColumn)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTemplateColumnRequest  $request
     * @param  \App\Models\TemplateColumn  $templateColumn
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTemplateColumnRequest $request, TemplateColumn $templateColumn)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TemplateColumn  $templateColumn
     * @return \Illuminate\Http\Response
     */
    public function destroy(TemplateColumn $templateColumn)
    {
        //
    }
}
