<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->bigInteger('priority_id')->unsigned();
            $table->bigInteger('template_column');
            $table->bigInteger('retrospective_id')->unsigned();
            $table->text('description');
            $table->timestamps();

            $table->foreign('priority_id')->references('id')->on('priorities')
                ->onDelete('cascade');

            $table->foreign('retrospective_id')->references('id')->on('retrospectives')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
