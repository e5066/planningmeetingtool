@extends('layouts.app')
<head>
    <link href="{{ asset('css/retrospective.css') }}" rel="stylesheet">
    <link href="{{ asset('css/cards.css') }}" rel="stylesheet">
</head>
@section('content')
    <div class="container-fluid" style="margin: 0 auto">
    <div class="retrospective card custom-card custom-card-border" style="width: 100%; height: auto;">
        <div class="card-header">
            <h5 class="text-white">{{$retroData->name}}</h5>
            <a class="btn btn-outline-light" href="{{ route('retrospective.create') }}" style="float: right">+</a>
        </div>
        <div class="card-body">
            @foreach($columns as $column)
                <?php
                    $cc = $column->id;
                ?>
                <div class="card custom-card custom-card-border col" style="background: none; box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px; height: 80%;">
                    <div class="card-header" style="margin-left:-20px; margin-right:-20px">
                        {{$column->name}}
                        <a class="btn btn-outline-light" href="{{ url('tasks/create/'.$retroData->id.'/'.$column->id.'/') }}" style="float: right">
                            +
                        </a>
                    </div>
                    @foreach($tasks as $task)
                        @if($task->template_column == $cc)
                    <div class="card-body">
                        <div class="card child-card custom-card-border" style="background: none; box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;">
                            <div class="card-header" style ="width: 100%; height: 40px">
                                {{$task->title}}
                                <a class="btn-sm btn-outline-light" href="{{ url('tasks/'.$task->id.'/edit/') }}" style="float: right">
                                    edit
                                </a>
                                <div class="card-body" style="margin-top: 20px; padding-bottom: 0px; height: auto; overflow-y: visible">
                                    <h7>Priority: {{$task->name}}</h7>
                                </div>
                                <div class="card-body" style="margin-top: 0px; height: auto; overflow-y: visible">
                                    <h5>{{$task->description}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                        @endif
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
    </div>
@endsection
