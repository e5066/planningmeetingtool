@extends('layouts.app')
<head>
    <link rel="stylesheet" href="{{asset('css/cards.css')}}">
</head>
@section('content')
    <div class="container-fluid pt-5" style="margin-left: 15%">
        <div class="row">
            <div class="custom-card col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 col-xxl-8 ps-4 pe-4 ps-sm-5 pe-sm-5 ps-md-5 pe-md-5 ps-lg-5 ps-xl-5 ps-xxl-5 pb-4">
                <div class="card custom-card-border mt-3 align-middle">
                    <h3 class="card-header">
                        Retrospective aanmaken
                    </h3>
                    <div class="card-body">
                        <form method="post" action="retrospective">
                            @csrf
                            <div class="form-group">
                                <label class="text-white">Naam van retrospective</label>
                                <input class="form-control" type="text" name="name">
                            </div>

                            <div class="form-group">
                                <label class="text-white">Voor welk team?</label>
                                <select class="form-control" name="team">
                                    @foreach($teams as $team)
                                        <option value="{{$team->id}}">{{$team->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="text-white">Welke template?</label>
                                <select class="form-control" name="template">
                                    @foreach($templates as $template)
                                        <option value="{{$template->id}}">{{$template->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <input class="form-control" type="submit" value="Aanmaken">
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
