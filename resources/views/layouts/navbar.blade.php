<head>
    <link rel="stylesheet" href="{{asset('css/navbar.css')}}">
</head>
<div class="container-fluid">
    <div class="row justify-content-start align-items-start">
        <div class="col-12">
            <nav class="navbar navbar-expand-lg navbar-dark">
                <div class="container-fluid">
                    <div class="col-12 col-sm-10 col-md-4 col-lg-4 col-xl-4 col-xxl-4 order-1 order-sm-0 order-md-1 order-lg-0 order-xl-0 order-xxl-0">
                        <a class="navbar-brand" href="#">
                            <img src="{{asset('images/logo.png')}}" alt="" width="100%" height="auto" class="d-inline-block align-text-top">
                        </a>
                    </div>
                    <div class="ham-menu mt-4 mt-sm-4 mt-md-0 mt-lg-0 mt-xl-0 mt-xxl-0 col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 col-xxl-4 align-self-start text-sm-right text-md-right text-center order-2 order-sm-2 order-md-0 order-lg-1 order-xl-1 order-xxl-1">
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link <?=(Route::current()->uri() == '/' ? 'active' : '')?> m-4 mt-0 pt-0" aria-current="page" href="/home">Home</a>
                                </li>
                                @auth
                                    @if(\App\Http\Controllers\RetrospectiveController::firstPossibleTeam() !== NULL)
                                <li class="nav-item">
                                    <a class="nav-link <?=(key(Route::current()->parameters) == 'team' ? 'active' : '')?> m-4 mt-0 pt-0" href="/teams/{{\App\Http\Controllers\RetrospectiveController::firstPossibleTeam()->name}}">Team</a>
                                </li>
                                    @else
                                        <li class="nav-item">
                                            <a class="nav-link <?=(key(Route::current()->parameters) == 'team' ? 'active' : '')?> m-4 mt-0 pt-0"
                                               href="{{route('teams.create')}}">Team</a>
                                        </li>
                                    @endif
                                @endauth
                                @auth
                                    @if(\App\Http\Controllers\RetrospectiveController::firstPossibleRetro() !== NULL)
                                <li class="nav-item">
                                    <a class="nav-link <?=(key(Route::current()->parameters) == 'retrospective' ? 'active' : '')?> m-4 mt-0 pt-0"
                                       href="/retrospective/{{\App\Http\Controllers\RetrospectiveController::firstPossibleRetro()->name}}">Retro</a>
                                </li>
                                        @else
                                        <li class="nav-item">
                                            <a class="nav-link <?=(key(Route::current()->parameters) == 'retrospective' ? 'active' : '')?> m-4 mt-0 pt-0"
                                               href="{{route('retrospective.create')}}">Retro</a>
                                        </li>
                                        @endif
                                        <li class="nav-item">
                                            <a class="nav-link m-4 mt-0 pt-0"
                                               href="{{route('logout')}}">Uitloggen</a>
                                        </li>
                                    @endauth
                            </ul>
                        </div>
                    </div>
                    <div class="account col-12 col-sm-2 col-md-4 col-lg-4 col-xl-4 col-xxl-4 text-left text-lg-right text-xl-right text-lg-right align-self-start order-0 order-sm-1 order-md-2 order-lg-2 order-xl-2 order-xxl-2">
                        <i class="bi bi-person-circle" style="font-size: 58px; color: white;"></i>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
