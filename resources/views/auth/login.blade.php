@extends('layouts.app')

@section('content')
<div class="container pt-5 pb-5">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <div class="card loginContainer">
                <div class="card-header text-center"><img src="{{URL::asset('images/logo.png')}}" width="50%" height="auto"/></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row justify-content-center">
                            <div class="col-md-6">
                                <div class="form-floating mb-3">
                                    <input id="email" type="email" placeholder="email" class="input-fields form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    <label for="email">Email address</label>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group row justify-content-center">
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input id="password" type="password" placeholder="password" class="input-fields form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                    <label for="password">Password</label>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group row justify-content-center">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <input class="form-check-input remember-check" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="input-fields form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0 justify-content-center">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary loginBtn m-3 me-4 ps-5 pe-5 pt-1 pb-1 mb-4">
                                    {{ __('Login') }}
                                </button>
                                <a href="register" class="btn btn-primary loginBtn m-3 ps-5 pe-5 pt-1 pb-1 mb-4">Register</a>
                            </div>
                        </div>

                        <div class="form-group row mb-0 justify-content-end float-right">
                            <div class="col-md-12">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link btn-reset" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
