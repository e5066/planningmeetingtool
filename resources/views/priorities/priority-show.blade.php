@extends('layouts.app')
@section('title','Priority Page')
<div style="height: max-content">
@section('content')

<h1 class="h1 text-white  pt-5 ps-3">Priority</h1>
<div class="container">
    <section class="mx-auto my-5" style="height: auto">
    <table class="table">
        <thead>
        <tr>
            <th class="text-white" scope="col" >Id</th>
            <th class="text-white" scope="col">Name</th>
            <th class="text-white" scope="col">color</th>
            <th class="text-white" scope="col">Update</th>
            <th class="text-white" scope="col">Delete</th>
           {{-- <th class="text-white" scope="col">action</th>--}}
        </tr>
        </thead>
        <tbody>
        @foreach($priorities as $priority)
        <tr>

            <th class="text-white" scope="row">{{$priority->id}}</th>
            <td><h4 class="h4" style="color: <?= $priority->color ?>">{{$priority->name}}</h4></td>
            <td class="text-white">{{$priority->color}}</td>

            {{--<td><a href="/priorities/{{$priority->id}}" class="btn btn-primary"> Show</a></td>--}}
            <td><a href="/priorities/{{$priority->id}}/edit" class="btn btn-light">update</a></td>
            <td><form action="/priorities/{{$priority->id}}" method="post">
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger" value="Delete">
            </form>
            </td>
             </tr>
        @endforeach
        </tbody>
    </table>
    </section>

       {{-- <h4 class="h4" style="color: <?='#' . $priority->color?>">{{$priority->name}}</h4>
        <h5>{{$priority->color}}</h5>

    <div class="card card-form mt-2 mb-4">
        <div class="card-header">{{$priority[0]->name}}</div>
        <div class="card-body rounded-top pink darken-4">
            <div class="d-flex justify-content-start align-items-center mb-4">
                <i class="far far-user fa-lg text-white fa-fw me-3"></i>

                <div class="form-outline form-white w-100">
                    <label class="form-label"
                        for="priorityname">Priority Name</label>
                    <input type="text" name="name"
                    id="priorityname"
                           class="form-control"
                           disabled
                                    value="{{$priority[0]->name}}">
                </div>
            </div>
        </div>
    </div>
</section>
</div>

--}}
</div>
@endsection
