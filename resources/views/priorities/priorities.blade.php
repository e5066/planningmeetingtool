{{--    <button type="button" class="btn btn-primary bg-secondary" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@mdo">priority for Task</button>--}}
    <div class="modal fade" id="modalPriority" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content bg-secondary">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">priority</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                    <form action="{{route('priorities.store')}}" method="post">
                        @csrf
                        <input name="teamName" type="hidden" value="{{$teams['teamInfo']['name']}}">
                        <input name="teamId" type="hidden" value="{{$teams['teamInfo']['id']}}">
                        <div class="mb-3">
                            <label  class="col-form-label">Priority:</label>
                            <input type="text" name="name" placeholder="Urgent">
                        </div>
                        <div class="mb-3">
                            <label for="color"> color:</label>
                            <input type="color" name="color" placeholder="#d70c0c">
                        </div>



                <div class="modal-footer">
                </div>

                        <input class="btn save-btn effect-btns" type="submit" value="Save priority">
                    </form>

                </div>
            </div>
        </div>
    </div>
