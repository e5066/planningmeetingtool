@extends('layouts.app')
@section('title','Priority Page')
<div style="height: max-content">
@section('content')
<h1 class="h1 text-white  pt-5 ps-3">Edit Priority</h1>
<div class="container">
    <form action="{{url('/priorities/'.$priority[0]->id)}}" method="post" class="pb-1 p-md-4">
        @csrf
        @method('patch')
        <div class="d-flex justify-content-start align-items-center mb-4">
            <i class="far far-user fa-lg text-white fa-fw me-3"></i>
            <div class="form-outline form-white w-50">
                <label class="form-label text-white" for="PriorityUpd">Priority name</label>
                <input type="text" name="name" id="PriorityUpd"
                class="form-control"
                value="{{$priority[0]->name}}"/>
                <label class="form-label text-white" for="colorUpd" >Priority color</label>
                <input type="color" name="color" id="colorUpd"
                       class="form-control"
                       value="{{$priority[0]->color}}"/>
            </div>
        </div>

        <div class="d-grid gap-2 d-md-block">
            <button class="btn btn-primary position-center" type="submit">Update</button>
        </div>

    </form>
</div>

@endsection
