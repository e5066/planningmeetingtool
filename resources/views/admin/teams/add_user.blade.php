<head>
    <link rel="stylesheet" href="{{asset('css/team.css')}}">
</head>
<form action="{{route('teams.store', $teams['teamInfo']['name'])}}" method="POST">
    @csrf
    @method('POST')
    <div class="modal fade" id="modalAddMember" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content bg-secondary">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Users</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <select name="selectedUser">
                        @foreach($teams['allUsers'] as $member)
                            <option value="{{$member->id}}">{{$member->name}}</option>
                        @endforeach
                    </select>
                    <input type="hidden" value="{{$teams['teamInfo']['id']}}" name="teamId"/>
                    <input type="hidden" value="{{$teams['teamInfo']['name']}}" name="teamName"/>
                </div>
                <div class="modal-footer">
                    <input name="addMember" class="btn save-btn effect-btns" type="submit" value="Add to Team">
                </div>
            </div>
        </div>
    </div>
</form>
