<div class="modal fade" id="modalAddTeam" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content bg-secondary">
            <div class="modal-header justify-content-center text-center">

                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{route('teams.create')}}" method="GET">
                    <div class="form-floating mb-3 justify-content-center text-center mt-5">
                        <h1 class="mb-5">Create a new team.</h1>
                        <input type="text" name="teamName" class="form-control" id="floatingInput" placeholder=""/>
                        @error('teamName')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                        <label for="floatingInput"></label>
                        <input type="submit" class="mt-5 btn save-btn2 effect-btns justify-content-center" value="Create"/>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
