@section('title', 'Team ' . $teams['teamInfo']['name'])
@extends('layouts.app')
<head>
    <link rel="stylesheet" href="{{asset('css/team.css')}}">
    <link rel="stylesheet" href="{{asset('css/cards.css')}}">
    <script src="{{asset('js/team.js')}}"></script>
</head>
@section('content')
    <audio id="soundtest" preload="auto">
        <source src="{{asset('sounds/menuSound.mp3')}}"/>
    </audio>
    <div class="container-fluid pt-5">
        @if(session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Updated:</strong> {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @elseif(session('failed'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Alert:</strong> {{ session('failed') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="row">
            <div class="custom-card col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 col-xxl-8 ps-4 pe-4 ps-sm-5 pe-sm-5 ps-md-5 pe-md-5 ps-lg-5 ps-xl-5 ps-xxl-5 pb-4">
                <div class="card custom-card-border mt-3 align-middle">
                    <h3 class="card-header row p-3 m-0 align-items-center">
                        <div class="col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8 col-xxl-8">
                            <span class="letter-icon me-3">{{$teams['teamInfo']['letterIcon']}}</span>{{$teams['teamInfo']['name']}}
                        </div>
                        <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 col-xxl-4">
                            <div class="dropdown">
                                <button class="btn add-retro-icon dropdown-toggle float-right pb-2 pt-2 ps-4 pe-4" type="button" id="dropdownMenuTeams" data-bs-toggle="dropdown" aria-expanded="false"></button>
                                <ul class="dropdown-menu teams-dropdown" aria-labelledby="dropdownMenuTeams">
                                    @foreach($subOnTeams as $team)
                                        <li class="pb-3 pt-2 pb-2">
                                            <a class="dropdown-item" href="{{route('teams.show', $team->name)}}"
                                                style="color: white; font-size: 24px; font-family: 'Montserrat', sans-serif">
                                                <span class="letter-icon me-3">{{$team->name[0]}}</span>{{$team->name}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </h3>
                    <div class="card-body">
                        <div class="row ms-4 me-4 mt-4 align-items-center justify-content-center">
                            <form method="post" action="{{route('teams.update', $teams['teamInfo']['id'])}}" class="d-flex row align-items-center">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="isAdmin" value="{{$teams['teamInfo']['members'][$userInArr]->isAdmin}}">
                                <div class="col-12 col-xl-2 pb-4 text-center">
                                    <span class="letter-icon-lg me-2 ms-2">{{$teams['teamInfo']['letterIcon']}}</span>
                                </div>
                                <div class="col-12 col-xl-8">
                                    <div>
                                        <div class="form-floating mb-3 m-0 m-sm-0 m-md-0 m-lg-4 m-xl-4 m-xxl-4 ps-lg-5 ps-xl-5 ps-xxl-5 pe-lg-5 pe-xl-5 pe-xxl-5">
                                            <input name="teamName" type="text" class="form-control team-name-input admin-control" id="floatingInput" placeholder="{{$teams['teamInfo']['name']}}">
                                            <label class="ms-lg-5 ms-xl-5 ms-xxl-5 label-team" for="floatingInput">{{$teams['teamInfo']['name']}}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-xl-2">
                                    <div class="row d-flex">
                                        <input name="teamId" type="hidden" value="{{$teams['teamInfo']['id']}}">
                                        <input name="saveTeam" class="btn save-btn effect-btns admin-control" type="submit" value="Save team">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="row mt-4">
                            <div class="child-card col-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 col-xxl-8 ps-lg-4 ps-xl-4 ps-xxl-4">
                                <div class="card custom-card-border mt-3 align-middle">
                                    <h3 class="card-header text-center">Retro's
                                        <a href="{{route('retrospective.create')}}" id="add-retro-icon" type="button" class="btn float-right p-0 admin-control">
                                            +
                                        </a>
                                    </h3>
                                    <div class="card-body">
                                            <div class="row m-3 align-items-center">
                                        @foreach($subsOnRetro as $retro)
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 col-xxl-6 text-center p-2">
                                                    <a class="retroLink" onmouseenter="play()" href="/retrospective/{{$retro->retroName}}"><h1 class="retroItem p-2">{{$retro->retroName}}</h1></a>
                                                </div>
                                        @endforeach
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="child-card col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4 col-xxl-4 ps-lg-4 ps-xl-4 ps-xxl-4 pe-lg-4 pe-xl-4 pe-xxl-4 pb-4">
                                <div class="card custom-card-border mt-3 align-middle">
                                    <h3 class="card-header text-center">Priorities
                                        <button id="add-retro-icon" type="button" class="btn float-right p-0 admin-control" data-bs-toggle="modal" data-bs-target="#modalPriority">
                                            +
                                        </button>
                                        @extends('priorities.priorities')
                                    </h3>
                                    <div class="card-body">
                                        @foreach($teams['priorities'] as $priority)
                                            <div class="row d-flex justify-content-center align-items-center pb-2">
                                                <div class="col-10 m-0 ps-3 pe-3 pt-0">
                                                    <h4 class="h4 priorities-cards text-center pt-2 pb-2" style="background-color: <?=$priority->color?>">{{$priority->name}}</h4>
                                                </div>
                                                <div class="col-2 m-0 p-0 admin-control">
                                                    <button class="admin-control btn" onclick="location.href = '{{route('priorities.show', $priority->id)}}'">
                                                            <i class="bi bi-three-dots-vertical prio-settings float-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row d-flex pt-2 justify-content-center ps-5 pe-5">
                            <button name="deleteTeam" class="btn delete-btn effect-btns admin-control" onclick="deleteTeam()">Delete team</button>
                            <form id="deleteTeam" action="{{route('teams.destroy', $teams['teamInfo']['id'])}}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="isAdmin" value="{{$teams['teamInfo']['members'][$userInArr]->isAdmin}}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4 ps-3 pe-3 ps-sm-3 pe-sm-3 ps-md-3 pe-md-3 ps-lg-3 ps-xl-5 ps-xxl-5">
                <div class="custom-card col-12">
                    <div class="card custom-card-border mt-3 align-middle">
                        <h3 class="card-header text-center">Members
                            <button id="add-retro-icon" type="button" class="btn float-right p-0 admin-control" data-bs-toggle="modal" data-bs-target="#modalAddMember">
                                +
                            </button>
                            @extends('admin.teams.add_user')
                        </h3>
                        <div class="card-body">
                            <div class="row ms-0 me-0 mt-3 align-items-center">
                                <div class="col-12">
                                    @foreach($teams['teamInfo']['members'] as $member)
                                        <div class="row pb-4 justify-content-center justify-content-sm-center justify-content-md-center justify-content-lg-between justify-content-xl-between justify-content-xxl-between align-items-center">
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4">
                                                <span class="letter-icon-md pe-3 ps-3" style="background: <?="rgb(". mt_rand(125, 255) . ',' .mt_rand(0, 255) . ',' . mt_rand(125, 255) . ");"?>">{{strtoupper($member->name[0])}}</span>
                                            </div>
                                            <div class="col-10 col-sm-10 col-md-10 col-lg-5 col-xl-5 col-xxl-5">
                                                <h1 class="text-left member-names">{{$member->name}}</h1>
                                            </div>
                                            <div class="col-2 col-sm-2 col-md-2 col-lg-3 col-xl-3 col-xxl-3">
                                                <div class="dropdown text-right">
                                                    <button class="btn admin-control" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                        <i class="bi bi-three-dots-vertical" style="font-size: 24px; color: white;"></i>
                                                    </button>
                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                        <form method="POST" class="m-0 p-0" action="{{route('teams.update', $teams['teamInfo']['id'])}}">
                                                            @csrf
                                                            @method('PUT')
                                                            <input type="hidden" name="userId" value="{{$member->user_id}}">
                                                            <input type="hidden" name="userName" value="{{$member->name}}">
                                                            <input type="hidden" name="isAdmin" value="{{$teams['teamInfo']['members'][$userInArr]->isAdmin}}">
                                                            <li><a class="dropdown-item" href="#"><input name="makeAdmin" value="Make admin" type="submit" class="btn"/></a></li>
                                                        </form>
                                                        <form class="m-0 p-0" method="POST" action="{{route('teams.destroy', $teams['teamInfo']['id'])}}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <input type="hidden" name="userId" value="{{$member->user_id}}">
                                                            <input type="hidden" name="userName" value="{{$member->name}}">
                                                            <input type="hidden" name="isAdmin" value="{{$teams['teamInfo']['members'][$userInArr]->isAdmin}}">
                                                            <li><a class="dropdown-item" href="#"><input name="removeUser" value="Remove" type="submit" class="btn"/></a></li>
                                                        </form>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function onload(){
            if({{$teams['teamInfo']['members'][$userInArr]->isAdmin}} == 0) {
                var adminControls = document.getElementsByClassName("admin-control");
                for (var i = 0; i < adminControls.length; i++) {
                    adminControls.item(i).disabled = true;
                }
            }
            else{
                var adminControls = document.getElementsByClassName("admin-control");
                for (var i = 0; i < adminControls.length; i++) {
                    adminControls.item(i).disabled = false;
                }
            }
        }
        window.onload=onload();
    </script>
@endsection
