@section('title', 'Home')
@extends('layouts.app')
<head>
    <link rel="stylesheet" href="{{asset('css/team.css')}}">
    <link rel="stylesheet" href="{{asset('css/cards.css')}}">
    <link rel="stylesheet" href="{{asset('css/team_overview.css')}}">
</head>
@section('content')
    <div class="container-fluid pt-5">
        <div class="row">
            <div class="custom-card col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 col-xxl-8 ps-4 pe-4 ps-sm-5 pe-sm-5 ps-md-5 pe-md-5 ps-lg-5 ps-xl-5 ps-xxl-5 pb-4">
                <div class="card custom-card-border mt-3 align-middle">
                    <h3 class="card-header">  TEAMS
                        <button id="add-retro-icon" type="button" class="btn float-right p-0" data-bs-toggle="modal" data-bs-target="#modalAddTeam">
                            + Create New Team
                        </button>
                        @extends('admin.teams.add_team')
                    </h3>
                    <div class="card-body">
                        <div class="row ms-4 me-4 mt-4 align-items-center justify-content-center">
                            @foreach($data['teams'] as $team)
                            <div class="child-card2 col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                                <div class="card custom-card-border2 mt-3 align-middle">
                                    <h1 class="text-left member-names ms-5 mb-2 mt-2">{{$team->name}}</h1>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="row mt-4 align-items-center">
                            <div class="child-card col-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 col-xxl-8 ps-lg-4 ps-xl-4 ps-xxl-4"></div>
                            <div class="child-card col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4 col-xxl-4 ps-lg-4 ps-xl-4 ps-xxl-4 pe-lg-4 pe-xl-4 pe-xxl-4"></div>
                        </div>
                        <div class="row d-flex pt-0 pb-5 justify-content-center ps-5 pe-5">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4 ps-3 pe-3 ps-sm-3 pe-sm-3 ps-md-3 pe-md-3 ps-lg-3 ps-xl-5 ps-xxl-5">
                <div class="custom-card col-12">
                    <div class="card custom-card-border mt-3 align-middle">
                        <h3 class="card-header text-center">Tasks
                        </h3>
                        <div class="card-body text-center">
                            @foreach($data['userTasks'] as $task)
                                <div class="child-card2 col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                                    <div class="card custom-card-border2 mt-3 align-middle">
                                        <h1 class="text-center member-names mt-2 mb-2">{{$task->title}}</h1>
                                    </div>
                                </div>
                            @endforeach
                            <div class="row ms-0 me-0 mt-3 align-items-center">
                                <div class="col-12">
                                        <div class="row pb-4 justify-content-center justify-content-sm-center justify-content-md-center justify-content-lg-between justify-content-xl-between justify-content-xxl-between align-items-center">
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4">
                                            </div>
                                            <div class="col-10 col-sm-10 col-md-10 col-lg-5 col-xl-5 col-xxl-5">
                                                <h1 class="text-left member-names"></h1>
                                            </div>
                                            <div class="col-2 col-sm-2 col-md-2 col-lg-3 col-xl-3 col-xxl-3">
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

