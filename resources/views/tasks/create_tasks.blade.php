@extends('layouts.app')
<head>
    <link rel="stylesheet" href="{{asset('css/cards.css')}}">
</head>
@section('content')
    <div class="container-fluid pt-5" style="margin-left: 15%">
        <div class="row">
            <div class="custom-card col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 col-xxl-8 ps-4 pe-4 ps-sm-5 pe-sm-5 ps-md-5 pe-md-5 ps-lg-5 ps-xl-5 ps-xxl-5 pb-4">
                <div class="card custom-card-border mt-3 align-middle">
                    <h3 class="card-header">
                        Task aanmaken
                    </h3>
                    <div class="card-body">
                        <form method="post" action="/tasks">
                            @csrf
                            <div class="form-group">
                                <input class="form-control" type="hidden" id="retid" name="retid" value="{{$retrospective}}">
                            </div>
                            <div class="form-group">
                                <label class="text-white">Retro Name</label>
                                <h4 style="color:white">{{$retname[0]->name}}</h4>
                            </div>
                            <div class="form-group">
                                <label class="text-white">Welke kolom?</label>
                                <select class="form-control" id="kolom" name="kolom">
                                    @foreach($columns as $column)
                                        <option value="{{$column->id}}">{{$column->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="text-white">Welke priority?</label>
                                <select class="form-control" id="priority" name="priority">
                                    @foreach($priorities as $priority)
                                        <option value="{{$priority->id}}">{{$priority->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="text-white">Title</label>
                                <input required class="form-control" type="text" id="title" name="title">
                            </div>
                            <div class="form-group">
                                <label class="text-white">Description</label>
                                <input required class="form-control" type="text" id="desc" name="desc">
                            </div>

                            <div class="form-group">
                                <input class="form-control" type="submit" value="Aanmaken">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
